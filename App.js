import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, StatusBar} from 'react-native';
import firebase from 'react-native-firebase';

class App extends React.Components {
  async componentDidMount() {
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          alert('Yes');
        } else {
          alert('No');
        }
      });
  }
  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <View>
            <Text>Debug</Text>
          </View>
          <View>
            <Text>Learn More</Text>
            <Text>Read the docs to discover what to do next:</Text>
            {firebase.messaging.nativeModuleExists && <Text>messaging()</Text>}
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default App;
